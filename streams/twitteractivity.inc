<?php
/**
 * @file
 * Twitter activity object.
 */

/**
 * Class TwitterActivity
 * Concrete class to build a stream with activity for twitter messages
 */
class TwitterActivity extends HeartbeatStream {

  /**
   * Implementation of queryAlter().
   */
  protected function queryAlter() {
    $this->query->condition('ha.access', 0, '>=');
    $this->query->condition('ha.message_id', 'heartbeat_twitter_add_tweet', '=');
  }

  /**
   * Function to add a part of a sql to a query built by views.
   *
   * @param object $view
   *   The view handler object by reference to add our part to the query
   */
  public function viewsQueryAlter(&$view) {
    $view->query->condition('ha.access', 0, '>=');
    $view->query->condition('ha.message_id', 'heartbeat_twitter_add_tweet', '=');
  }

}
