Instant integration between the Twitter module and the Heartbeat modules.

Status updates imported by the Twitter module are added to the site's Heartbeat
activity stream as well as a Twitter stream.
